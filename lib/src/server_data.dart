///Fields required to send
class WSEventData {
  ///Key
  String eventName;

  ///Key
  int initiatorID;

  ///Key
  String initiatorType;

  ///Key
  int initiatorAge;

  ///Key
  String initiatorGender;

  ///Key
  String initiatorCity;

  ///Key
  String initiatorCountry;

  ///Key
  int targetID;

  ///Key
  String targetType;

  ///Key
  String notes;

  ///Key
  int targetOwnerID;

  ///Key
  String targetOwnerType;

  ///Making a JSON type object
  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      eventNameKey: eventName,
      initiatorIDKey: initiatorID,
      initiatorTypeKey: initiatorType,
      initiatorAgeKey: initiatorAge,
      initiatorGenderKey: initiatorGender,
      initiatorCityKey: initiatorCity,
      initiatorCountryKey: initiatorCountry,
      targetIDKey: targetID,
      targetTypeKey: targetType,
      targetOwnerIDKey: targetOwnerID,
      targetOwnerTypeKey: targetOwnerType,
      notesKey: notes
    };
  }
}

///Key
const String eventNameKey = "event_name";

///Key
const String initiatorIDKey = "initiator_id";

///Key
const String initiatorTypeKey = "initiator_type";

///Key
const String initiatorAgeKey = "initiator_age";

///Key
const String initiatorGenderKey = "initiator_gender";

///Key
const String initiatorCityKey = "initiator_city";

///Key
const String initiatorCountryKey = "initiator_country";

///Key
const String targetIDKey = "target_id";

///Key
const String targetTypeKey = "target_type";

///Key
const String notesKey = "notes";

///Key
const String targetOwnerIDKey = "target_owner_id";

///Key
const String targetOwnerTypeKey = "target_owner_type";

///Http methods
enum HttpMethod {
  ///Http method GET
  get,

  ///Http method POST
  post,

  ///Http method PATCH
  patch,

  ///Http method PUT
  put,

  ///Http method DELETE
  delete,
}

///Authentication types
enum AuthType {
  ///Authentication with bearer token
  bearerToken,

  ///Authentication with access token
  accessToken,
}
