import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:ws_stats/src/server_data.dart';

///Sending statistics object to server
class WSStats {
  ///Singleton constructor
  factory WSStats() {
    return _wsStats;
  }

  ///Internal constructor
  WSStats._internal();

  ///Url base
  String baseUrl;

  ///Token string
  String token;

  ///Dio instance
  Dio _dio;

  ///Instance
  static final WSStats _wsStats = WSStats._internal();

  ///Instance provider
  static WSStats get instance {
    return _wsStats;
  }

  ///Logger
  final Logger _logger =
      Logger(printer: PrettyPrinter(printEmojis: false, printTime: true));

  ///Getter for [Logger]
  Logger get logger {
    return instance._logger;
  }

  ///Log event
  Future<void> logEvent(WSEventData serverData) async {
    if (_dio == null) {
      _dio = _createDio();
    }
    var options =
        await createOptions(needAuth: true, authType: AuthType.bearerToken);
    var data = {"stat": serverData};
    await _dio.post(baseUrl, options: options, data: data);
    _logger.d(" ==== Event ${serverData.eventName} event is logged ==== ");
  }

  ///Initialize Dio
  Dio _createDio() {
    var dio = Dio();
    dio.interceptors.add(
      PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: false,
          error: true,
          compact: true,
          maxWidth: 180),
    );
    dio.options.connectTimeout = 15000;
    dio.options.receiveTimeout = 30000;
    return dio;
  }

  ///Create options for request
  Future<Options> createOptions({bool needAuth, AuthType authType}) async {
    var headers = <String, dynamic>{};
    headers["Accept"] = "application/json";
    if (needAuth) {
      if (token == null || token == "") {
        throw DioError(error: "Authorization token required");
      } else {
        switch (authType) {
          case AuthType.bearerToken:
            _logger.d("Adding bearer token :: $token");
            headers["Authorization"] = "Bearer $token";
            break;
          case AuthType.accessToken:
            _logger.d("Adding access token :: $token");
            headers["Access_token"] = token;
        }
      }
    }
    var result = Options(headers: headers);
    return result;
  }
}
